Dynamic Internal Linking
========================

Description
-----------
This module gives functionality converting the some selected keywords to links
in the content. This is good for the page ranks. User can convert atmost 5
keywords. Currently this module focuses only on textareas.

Requirements
------------
Drupal 7.x

Installation
------------

1. Download the module and place in sites/all/modules directory OR use
drush dl dynamic_internal_linking
2. Login as administrator. Enable the module in the "Administer" -> "Modules" OR
use drush en dynamic_internal_linking

Functionality
-------------

1. Convert the keywords into the links in a textarea field
